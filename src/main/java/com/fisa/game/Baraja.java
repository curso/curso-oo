package com.fisa.game;

public class Baraja {
	
	public static final int CARTAS_POR_PALO = 13;
	public static final int TOTAL_CARTAS = CARTAS_POR_PALO * Palo.values().length; 
	
	Carta[] cartas;
	int cartaActual = 0;
	
	public Baraja() {
		cartas = new Carta[TOTAL_CARTAS];
		for(byte i = 0; i < 13; i++){
			for(Palo palo : Palo.values()){
				int indice = palo.ordinal() * CARTAS_POR_PALO + i; 
				cartas[indice] = new Carta ((byte)(i+1), palo);
			}
		}
	}
	
	void recorrer(){
		System.out.print("\n");
		for(Carta carta : cartas){
			System.out.print(carta.numero+" de "+carta.palo.name()+"\t");
		}
		System.out.print("\n");
	}
	
	void barajar(){
		for(int i = 0; i<TOTAL_CARTAS ; i++){
			int posicion = (int) (Math.random() * TOTAL_CARTAS);
			Carta actual = cartas[i];
			Carta reposicionada = cartas[posicion];
			cartas[i] = reposicionada;
			cartas[posicion] = actual;
		}
	}
	
	Carta voltear(){
		Carta carta = null;
		if(cartaActual < TOTAL_CARTAS){
			carta = cartas[cartaActual];
			cartaActual++;
		}
		return carta;
	}
	
	
	Carta[] repartir (int numero) {
		if(numero < TOTAL_CARTAS){
			Carta[] cartas = new Carta[numero];
			if(cartaActual + numero < TOTAL_CARTAS){
				for(int i = 0; i < numero; i++){
					Carta repartida = voltear();
					cartas[i] = repartida;
				}
			}
			return cartas;
		}
		return null;
	}
	
}
