package com.fisa.game;

/**
 * Hello world!
 *
 */
public class CartaIntermedia 
{
    public static void main( String[] args )
    {
        System.out.println( "Bienvenido" );
        Baraja baraja = new Baraja();
        baraja.recorrer();
        baraja.barajar();
        baraja.recorrer();
        Carta carta = baraja.voltear();
        System.out.println("CARTA "+carta.numero+" "+carta.palo);
        carta = baraja.voltear();
        System.out.println("CARTA "+carta.numero+" "+carta.palo);
        
        Carta[] cartas = baraja.repartir(5);
        for(Carta c : cartas){
        	System.out.println("CARTA "+c.numero+" "+c.palo);
        }
    }
}
